﻿﻿using System;
using System.Threading;
using System.Windows.Threading;

namespace DebitExpress.SplashScreen.Wpf
{
    public class SplashScreenManager<T> where T : SplashScreenWindow, new()
    {
        private readonly string _threadName;
        private readonly string _initialStatus;
        private SplashScreenWindow _splashScreen;
        private ManualResetEvent _resetSplashCreated;

        /// <summary>
        /// Create an instance of <see cref="SplashScreenManager{T}"/>
        /// </summary>
        /// <param name="initialStatus"></param>
        public SplashScreenManager(string initialStatus = "Initializing...")
        {
            _initialStatus = initialStatus;
            _threadName = Guid.NewGuid().ToString();
        }

        /// <summary>
        /// Start the process of splash screen from different thread.
        /// </summary>
        public void Start()
        {
            _resetSplashCreated = new ManualResetEvent(false);

            var splashThread = new Thread(ShowSplash)
            {
                Name = _threadName,
                IsBackground = true
            };
            
            splashThread.SetApartmentState(ApartmentState.STA);
            splashThread.Start();

            _resetSplashCreated.WaitOne();
        }

        private void ShowSplash()
        {
            _splashScreen = new T();
            _splashScreen.SetStatus(_initialStatus);
            _splashScreen.Show();
            _resetSplashCreated.Set();
            
            Dispatcher.Run();
        }
        
        /// <summary>
        /// Update status message.
        /// </summary>
        /// <param name="statusMessage"></param>
        public void UpdateStatus(string statusMessage)
        {
            _splashScreen.SetStatus(statusMessage);
        }

        /// <summary>
        /// Stop splash screen process.
        /// </summary>
        public void Stop()
        {
            _splashScreen.LoadComplete();
        }
    }
}