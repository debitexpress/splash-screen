﻿﻿using ControlzEx.Behaviors;
using Microsoft.Xaml.Behaviors;
using System.Windows;

namespace DebitExpress.SplashScreen.Wpf
{
    public abstract class SplashScreenWindow : Window
    {
        static SplashScreenWindow()
        {
            DefaultStyleKeyProperty.OverrideMetadata(typeof(SplashScreenWindow),
                new FrameworkPropertyMetadata(typeof(SplashScreenWindow)));
        }

        protected SplashScreenWindow()
        {
            Closing += OnClosing;
            WindowStartupLocation = WindowStartupLocation.CenterScreen;
            SetResourceReference(StyleProperty, typeof(SplashScreenWindow));
            InitializeWindowChromeBehavior();
        }

        private void InitializeWindowChromeBehavior()
        {
            var chromeBehavior = new WindowChromeBehavior();
            Interaction.GetBehaviors(this).Add(chromeBehavior);
        }
        
        public static readonly DependencyProperty StatusProperty =
            DependencyProperty.Register(nameof(Status), typeof(string), typeof(SplashScreenWindow));

        public string Status
        {
            get => (string)GetValue(StatusProperty);
            set => SetValue(StatusProperty, value);
        }

        private void OnClosing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            e.Cancel = true;
        }

        public void SetStatus(string message)
        {
            Dispatcher.Invoke(() => Status = message);
        }

        public void LoadComplete()
        {
            Dispatcher.InvokeShutdown();
        }
    }
}