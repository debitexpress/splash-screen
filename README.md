# DebitExpress Splash Screen

### Definition
	A thread-safe API for showing splash screen when WPF application starts.

### Usage
1. Create a window which inherits from SplashScreenWindow. *(see sample xaml code below)*
    ```xaml
   <splash:SplashScreenWindow
       x:Class="DebitExpress.Demo.DemoSplashScreen"
       xmlns="http://schemas.microsoft.com/winfx/2006/xaml/presentation"
       xmlns:x="http://schemas.microsoft.com/winfx/2006/xaml"
       xmlns:splash="clr-namespace:DebitExpress.SplashScreen.Wpf;assembly=DebitExpress.SplashScreen.Wpf"
       Title="Splash Screen"
       Height="300"
       Width="500">
       <Grid>
           <StackPanel 
               VerticalAlignment="Center"
               HorizontalAlignment="Center">
               <TextBlock 
                   Text="This is a sample splash screen"/>
               <TextBlock
                   Margin="0,10,0,0"
                   Text="{Binding Status, RelativeSource={RelativeSource AncestorType={x:Type splash:SplashScreenWindow}}}" />
               <ProgressBar
                   IsIndeterminate="True" />
           </StackPanel>
       </Grid>
   </splash:SplashScreenWindow> 
   ```
2. Remove default StartupUri in App.xaml and merge SplashScreen.xaml as resource dictionary
    ```xaml
    <Application.Resources>
        <ResourceDictionary>
            <ResourceDictionary.MergedDictionaries>
                 <ResourceDictionary
                      Source="pack://application:,,,/DebitExpress.SplashScreen.Wpf;component/SplashScreen.xaml" />
                 <!-- other code goes here... -->
            </ResourceDictionary.MergedDictionaries>
        </ResourceDictionary>
    </Application.Resources>
    
    ```
3. Show splash screen using splash screen manager in App.cs
    ```c#
    public partial class App
    {
        private readonly SplashScreenManager<DemoSplashScreen> _splashScreenManager;
        
        public App()
        {
            _splashScreenManager = new SplashScreenManager<DemoSplashScreen>();
        }
    
        protected override void OnStartup(StartupEventArgs e)
        {
            _splashScreenManager.Start();
    
            //[time consuming process goes here...]
            
            _splashScreenManager.UpdateStatus("Finishing up...");
            
            var view = new MainWindow();
            view.Loaded += ViewOnLoaded;
            Current.MainWindow = view;
            view.Show();
        }
    
        private void ViewOnLoaded(object sender, RoutedEventArgs e)
        {
            _splashScreenManager.UpdateStatus("Opening...");
    
            //[another time consuming process goes here...]
            
            _splashScreenManager.Stop();
    
            var mainWindow = sender as Window;
            mainWindow?.Activate();
        }
    }
    ```

### External Dependencies
- [ControlzEx](https://github.com/ControlzEx/ControlzEx)

### Company

> DebitExpress Business Solutions

### Author(s)
- Ogie Galicia, CPA
